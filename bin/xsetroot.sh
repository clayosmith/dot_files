#/bin/sh
echo "$(nmcli | grep ": connected" | cut -d " " -f1,4 | tr -d ' ') | $(date +"%a, %b %d | %I:%M %p |") $(pacmd list-sinks | awk 'NR==9{print $5}') volume | $(cat /sys/class/power_supply/BAT0/status) $(cat /sys/class/power_supply/BAT0/capacity)"
