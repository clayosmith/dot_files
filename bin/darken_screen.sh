#!/bin/sh
# darkest setting is 1 that i do feel comfortable with it being at in extreme cases

current_brightness=$(cat /sys/class/backlight/intel_backlight/brightness)

if (( $current_brightness == 1 ));
    then xsetroot -name "                          ALREADY AT MINIMUM BRIGHTNESS                          " && new_brightness=1;
elif (( $current_brightness > 301 ));
    then new_brightness=$(( $current_brightness - 300 )); 
else new_brightness=1;
fi


echo $new_brightness > /sys/class/backlight/intel_backlight/brightness
