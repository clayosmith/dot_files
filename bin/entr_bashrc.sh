#!/bin/bash

(echo /home/otis3761/.bashrc | entr -p cp /home/otis3761/.bashrc /home/otis3761/code/git/dot_files/config_files/.bashrc &)
(echo /home/otis3761/.bashrc | entr -p notify-send -t 2000 ".bashrc updated" &) 
(echo /home/otis3761/code/git/dot_files/config_files/.bashrc | entr -p git --git-dir=/home/otis3761/code/git/dot_files/.git --work-tree=/home/otis3761/code/git/dot_files/ commit -a -m "This was done automatically by a script." &)
