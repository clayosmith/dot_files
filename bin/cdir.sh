#!/bin/bash
#getopts command
#Have flags for locate vs find, fzf vs dmenu, vs rofi and one to use updatedb if they are using locate (similar to big and small flags for pacman)

# Do they have the locate command?
#       Yes they have the locate command
#           Do they have fzf, dmenu or rofi?
#               If they only have one of the 3, use it.
#                   If they have multiple, check for flags.
#                      If they used a flag check to see if it matches one in which they have installed
#                           if it does, use it and put it in a variable.
#                           if it doesnt, kill the program and tell them why
#                       If they didnt use any flags use them in this order by which is installed; fzf, dmenu, then rofi, put it in a variable
#                   If they dont have any of them, kill the program and tell them why
#               Did they give any arguments (keywords for a file or directory name?)
#                   Yes, use it by using its positional argument
#                   No, then use / * as a place holder

#       No they do not have the locate command, use the find command
#           Do they have fzf, dmenu or rofi?
#               If they only have one of the 3, use it.
#                   If they have multiple, check for flags.
#                       If they used a flag check to see if it matches one in which they have installed
#                           if it does, use it and put it in a variable.
#                           if it doesnt, kill the program and tell them why
#                       If they didnt use any flags use them in this order by which is installed; fzf, dmenu, then rofi, put it in a variable
#                   If they dont have any of them, kill the program and tell them why
#               Did they give any arguments (keywords for a file or directory name?)
#                   Yes, use it by using its positional argument
#                   No, then use / * as a place holder

# Use the file command to see if the location they chose was a file or a directory
#   If the location they chose was a directory, cd there without using dirname
#   If the location they chose was not a directory, cd there with dirname











#   dir_or_not=$(file -b $1)
#   depcheck_locate=$(command -v locate)
#   depcheck_fzf=$(command -v fzf)
#   depcheck_dmenu=$(command -v dmenu)
#   depcheck_rofi=$(command -v rofi)


#   if [[ $depcheck_locate != "" ]];
#       then if [[ $depcheck_fzf != "" ]];
#               then cd "$(dirname $(locate $1 | grep -v "%\|~\|cache" | fzf))"
#           elif [[ $depcheck_dmenu != "" ]]; 
#               then cd "$(dirname $(locate $1 | grep -v "%\|~\|cache" | dmenu -l 10 | xargs) 2>/dev/null)"
#           elif [[ $depcheck_rofi != "" ]];
#               then cd "$(dirname $(locate $1 | grep -v "%\|~\|cache" | rofi -dmenu | xargs))"
#           else printf "You have the locate command, but you are still missing; dmenu, fzf, and rofi in which at least one is needed to use this command!\n"
#       fi
#   elif [[ $depcheck_fzf != "" ]];
#       then cd "$(dirname $(locate $1 | grep -v "%\|~\|cache" | fzf))"
#   elif [[ $depcheck_dmenu != "" ]];
#       then cd "$(dirname $(sudo find / -name *$1* 2>&1 | grep -v "Permission\|%" | dmenu -l 10))"
#   elif [[ $depcheck_rofi != "" ]];
#       then cd $(dirname $(sudo find / -name *$1* 2>&1 | grep -v "Permission\|%\|cache" | rofi -dmenu))
#   else printf "You have none of the required software"
#   fi
