#!/bin/sh
# 4437 = max brightness (cant go higher than that without an error)

# unset current_brightness
# unset new_brightness


current_brightness=$(cat /sys/class/backlight/intel_backlight/brightness)

if (( $current_brightness == 4437 ));
    then xsetroot -name "                          ALREADY AT MAX BRIGHTNESS                          " && new_brightness=4437;
elif (( $current_brightness < 4137 ));
    then new_brightness=$(( $current_brightness + 300 )); 
else new_brightness=4437;
fi

echo $new_brightness > /sys/class/backlight/intel_backlight/brightness
