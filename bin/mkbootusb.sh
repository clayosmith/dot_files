#!/bin/bash
read -p "CAUTION this will WIPE and format the chosen drive! Press ENTER to continue or Control + c to exit." ENTER

chosen_device=$(ls /dev/ | grep "mmc\|nvme\|sr\|sd" | dmenu -l 10 -p "Choose device") &&

sudo updatedb

chosen_image=$(locate *.iso | dmenu -l 10 -p "Choose image") &&

read -p "Are you sure you want to put $chosen_image on $chosen_device? " Yn &&

yes_if1=Y
yes_if2=y
yes_if3=yes
yes_if4=YES
yes_if5=Yes
yes_if6=yES
yes_if7=yEs
yes_if8=YeS
yes_if9=YEs

    if [ "$Yn" = "$yes_if1" ];
then
    sudo dd of="/dev/$chosen_device" if="$chosen_image" status=progress

   elif [ "$Yn" = "$yes_if2" ];
then
   sudo dd of="/dev/$chosen_device" if="$chosen_image" status=progress

   elif [ "$Yn" = "$yes_if3" ];
then
   sudo dd of="/dev/$chosen_device" if="$chosen_image" status=progress

   elif [ "$Yn" = "$yes_if4" ];
then
   sudo dd of="/dev/$chosen_device" if="$chosen_image" status=progress

   elif [ "$Yn" = "$yes_if5" ];
then
   sudo dd if="$chosen_image" of="/dev/$chosen_device" status=progress

   elif [ "$Yn" = "$yes_if6" ];
then
   sudo dd if="$chosen_image" of="/dev/$chosen_device" status=progress

   elif [ "$Yn" = "$yes_if7" ];
then
   sudo dd if="$chosen_image" of="/dev/$chosen_device" status=progress

   elif [ "$Yn" = "$yes_if8" ];
then
   sudo dd if="$chosen_image" of="/dev/$chosen_device" status=progress

   elif [ "$Yn" = "$yes_if9" ];
then
   sudo dd if="$chosen_image" of="/dev/$chosen_device" status=progress
else
    echo "The program STOPPED"
fi
