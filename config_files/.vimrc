"    o8o               o8o      .                    o8o                    
"    `"'               `"'    .o8                    `"'                    
"   oooo  ooo. .oo.   oooo  .o888oo     oooo    ooo oooo  ooo. .oo.  .oo.   
"   `888  `888P"Y88b  `888    888        `88.  .8'  `888  `888P"Y88bP"Y88b  
"    888   888   888   888    888         `88..8'    888   888   888   888  
"    888   888   888   888    888 .  .o.    `888'     888   888   888   888  
"   o888o o888o o888o o888o   '888'  Y8P     `8'     o888o o888o o888o o888o

"Plugins that I use
call plug#begin()
Plug 'tpope/vim-repeat'
Plug 'justinmk/vim-sneak'
Plug 'tpope/vim-surround'
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-commentary'
Plug 'bronson/vim-visual-star-search'
Plug 'mbbill/undotree'
Plug 'matze/vim-move'
Plug 'tpope/vim-fugitive'
Plug 'easymotion/vim-easymotion'
Plug 'vifm/vifm.vim'
Plug 'junegunn/fzf.vim'
Plug 'romainl/vim-cool'
Plug 'wellle/targets.vim'
Plug 'chrisbra/NrrwRgn'
Plug 'reedes/vim-wordy'
Plug 'reedes/vim-lexical'
Plug 'skywind3000/asyncrun.vim'
Plug 'whiteinge/diffconflicts'
Plug 'itchyny/calendar.vim'
Plug 'mtth/scratch.vim'
Plug 'chrisbra/csv.vim'
Plug 'tommcdo/vim-exchange'
Plug 'pbrisbin/vim-mkdir'
Plug 'dhruvasagar/vim-table-mode'
Plug 'yuttie/comfortable-motion.vim'
Plug 'junegunn/vim-easy-align'
Plug 'andrewradev/linediff.vim'
Plug 'bogado/file-line'
Plug 'valloric/listtoggle'
Plug 'will133/vim-dirdiff'
Plug 'simeji/winresizer'
Plug 'blueyed/vim-diminactive'
Plug 'tpope/vim-eunuch'
Plug 'wesq3/vim-windowswap'
Plug 'mg979/vim-visual-multi'
"Plug 'neoclide/coc.nvim'
"
Plug 'junegunn/vim-peekaboo'
Plug 'vimwiki/vimwiki'
Plug 'mhinz/vim-startify'
Plug 'jacquesbh/vim-showmarks'
"
Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase' }
Plug 'machakann/vim-highlightedyank'
Plug 'junegunn/goyo.vim'
Plug 'bpstahlman/txtfmt'
Plug 'mhinz/vim-signify'
Plug 'mizux/vim-colorschemes'
Plug 'unblevable/quick-scope'
"
Plug 'pbrisbin/vim-colors-off'
Plug 'owickstrom/vim-colors-paramount'
Plug 'fxn/vim-monochrome'
Plug 'junk-e/identity.vim'
Plug 'duckwork/nirvana'
Plug 'Jorengarenar/vim-darkness'
Plug 'kyoto-shift/film-noir'
Plug 'matveyt/vim-modest'
Plug 'neutaaaaan/monosvkem'
call plug#end()

"Plugin Setup
let mapleader=" "
let g:sneak#label = 1
let g:signify_realtime = 1
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
let g:vimwiki_list = [{'path': '/home/otis3761/files/documents/vimwiki', 'path_html': '~/public_html/'}]
let g:rainbow_active = 1
let g:syntastic_aggregate_errors = 1
let g:ackprg = 'doas -- ag --hidden --vimgrep'
let g:Hexokinase_highlighters = ['foregroundfull']
let g:move_key_modifier = 'C'
let g:lt_location_list_toggle_map = '<leader>Q'
let g:lightline = {
    \ 'component': {
    \   'filename': '%F',
    \  }
    \  }
hi ActiveWindow ctermbg=16 | hi InactiveWindow ctermbg=233
set winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow

"autocommands/au 
autocmd VimEnter *.csv :execute "norm VG"
autocmd FocusGained *.csv :ArrangeColumn
autocmd BufNewFile,BufRead *.mom nnoremap <F9> :w<CR>:! groff -m mom -Tpdf % > %:r_mom.pdf<CR><CR>
autocmd BufNewFile,BufRead *.mom nnoremap <F10> :! nohup zathura %:r_mom.pdf &<CR><CR>
autocmd BufNewFile,BufRead *.mom :set noai spell spelllang=en_us textwidth=96
autocmd FileType tex nnoremap <F10> :! nohup zathura %:r.pdf &<CR><CR>
autocmd FileType tex nnoremap <F9> :w<CR>:! pdflatex %<CR><CR>
autocmd FileType tex nnoremap JJ /zxc<CR>cw
autocmd FileType tex inoremap <C-i> \textit{}<Esc>i
autocmd FileType tex inoremap <C-b> \textbf{}<Esc>i
autocmd FileType tex inoremap JJ <Esc>/zxc<CR>cw


highlight QuickScopePrimary ctermbg=RED cterm=underline
highlight QuickScopeSecondary ctermbg=81 cterm=underline


"REMAPS
"command-mode remaps
cnoremap vifm VsplitVifm<CR>
cnoremap CRR :let @a=''<CR>:let @b=''<CR>:let @c=''<CR>:let @d=''<CR>:let @e=''<CR>:let @f=''<CR>:let @g=''<CR>:let @h=''<CR>:let @i=''<CR>:let @j=''<CR>:let @k=''<CR>:let @l=''<CR>:let @m=''<CR>:let @n=''<CR>:let @o=''<CR>:let @p=''<CR>:let @q=''<CR>:let @r=''<CR>:let @s=''<CR>:let @t=''<CR>:let @u=''<CR>:let @v=''<CR>:let @w=''<CR>:let @x=''<CR>:let @y=''<CR>:let @z=''<CR>:let @0=''<CR>:let @1=''<CR>:let @2=''<CR>:let @3=''<CR>:let @4=''<CR>:let @5=''<CR>:let @6=''<CR>:let @7=''<CR>:let @8=''<CR>:let @9=''<CR>:let @/=''<CR>:let @"=''<CR>:let @-=''<CR>
cnoremap DM delmark abcdefghijklmnopqrstuvwxyz0123456789><][^.<CR>:delmark \"<CR>
cnoremap PI PlugInstall<CR>
cnoremap PC PlugClean<CR>
cnoremap :q :q!


"insert-mode remaps
inoremap ,, <Esc>la

"normal-mode remaps
nnoremap j gj
nnoremap k gk
nnoremap :q :q!
nnoremap ~~~ :set background=light<CR>
nnoremap ~~ :set background=dark<CR>
nnoremap <F2> :w<CR>:source /home/otis3761/.config/nvim/init.vim<CR>
nnoremap m :mark 
nnoremap <leader>~~ :set notermguicolors<CR>
nnoremap <leader>~ :set termguicolors<CR>
nnoremap <leader><backSpace> :VsplitVifm<CR>
nnoremap <leader>f :Files /home/otis3761<CR>
nnoremap <leader>F :Files /<CR>
nnoremap <leader>b :ls<CR>:b
nnoremap <leader>h :bp<CR>
nnoremap <leader>l :bn<CR>
nnoremap <leader>H :tabp<cr>
nnoremap <leader>L :tabn<cr>
nnoremap <leader>e :tabedit 
nnoremap <leader>a :Ack!<space>
nnoremap <leader>w <C-w>
nnoremap <leader>m :DoShowMarks!<cr>
nnoremap <leader>M :NoShowMarks!<cr>
nnoremap <leader>G :Goyo 150x50<cr>
nnoremap <leader>vs :ls<cr>:vert sb
nnoremap <leader>lv :loadview<cr>
nnoremap <leader>ww :call WindowSwap#EasyWindowSwap()<CR>
nnoremap <leader>ad :<Plug>(ale_detail)
nnoremap <leader>mv :mkview<cr>
nnoremap <leader>dv :Delview<cr>
nnoremap <F5> :UndotreeToggle<cr>
nnoremap <leader>sw :w<CR>:source %<CR>
nmap ga <Plug>(EasyAlign)
nmap <leader>p <Plug>yankstack_substitute_older_paste
nmap <leader>P <Plug>yankstack_substitute_newer_paste

"root mappings
map s <Plug>Sneak_s
map S <Plug>Sneak_S
map <leader>s <Plug>(wildfire-quick-select)

"Visual Mode Mappings
xmap ga <Plug>(EasyAlign)

"General settings I want nearly everytime I open a file
"set cursorcolumn colorcolumn=100
set undofile undodir=/home/otis3761/.local/share/nvim/undodir/
set mouse=a history=10000 updatetime=100 
set wildmenu wildmode=longest:full,full
set tabstop=4 shiftwidth=4 expandtab smarttab
set smartcase incsearch hlsearch ignorecase
set noswapfile nobackup hidden
set cursorline
set splitbelow splitright
set number relativenumber
set nowrap ruler 
set background=dark
set notermguicolors
set laststatus=0
set foldmethod=marker

"enable syntax and define colorscheme
filetype on
syntax enable
colorscheme clays_pencil_rewrite

" # Function to permanently delete views created by 'mkview'
function! MyDeleteView()
let path = fnamemodify(bufname('%'),':p')
" vim's odd =~ escaping for /
let path = substitute(path, '=', '==', 'g')
if empty($HOME)
else
let path = substitute(path, '^'.$HOME, '\~', '')
endif
let path = substitute(path, '/', '=+', 'g') . '='
" view directory
let path = &viewdir.'/'.path
call delete(path)
echo "Deleted: ".path
endfunction
" # Command Delview (and it's abbreviation 'delview')
command Delview call MyDeleteView()
" Lower-case user commands: http://vim.wikia.com/wiki/Replace_a_builtin_command_using_cabbrev
cabbrev delview <c-r>=(getcmdtype()==':' && getcmdpos()==1 ? 'Delview' : 'delview')<CR>
