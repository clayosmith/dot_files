#     .o8                          oooo                           
#    "888                          `888                           
#     888oooo.   .oooo.    .oooo.o  888 .oo.   oooo d8b  .ooooo.  
#     d88'`88b `P  )88b  d88(  "8  888P"Y88b  `888""8P d88'`"Y8 
#     888   888  .oP"888  `"Y88b.   888   888   888     888       
# 000 888   888 d8(  888  o.  )88b  888   888   888     888   .o8 
# 000 `Y8bod8P'`Y888""8o 8""888P'o888o o888o d888b    `Y8bod8P'

#Startx on bash startup via tty1?
# If not running interactively, don't do anything
#if systemctl -q is-active graphical.target && [[ ! $DISPLAY && $XDG_VTNR -eq 1 ]]; then
#    exec startx
#fi
[[ $- != *i* ]] && return

#Aliases
alias vimrc='nvim /home/otis3761/.config/nvim/init.vim'
alias bashrc='nvim /home/otis3761/.bashrc'
alias sxhkdrc='nvim /home/otis3761/.config/sxhkd/sxhkdrc'
alias xinitrc='nvim /home/otis3761/.xinitrc'
alias alacrittyconfig='nvim ~/.config/alacritty/alacritty.yml'
alias qbconfig='vim /home/otis3761/.config/qutebrowser/config.py'

alias l='ls -1F --color=auto --group-directories-first'
alias la='ls -1AF --color=auto --group-directories-first'
alias lsa='ls -1AF --color=auto --group-directories-first'
alias lsl='ls -lF --color=auto --group-directories-first'
alias lsla='ls -lAF --color=auto --group-directories-first'
alias mkdir='mkdir -pv'

alias p='pacman'
alias sps='pacman -S --needed'
alias spss='pacman -Ss'
alias spq='pacman -Q > /home/otis3761/code/git/dot_files/random/installed_all.txt &&  pacman -Q'
alias spqe='pacman -Qeq &&  pacman -Qeq > /home/otis3761/code/git/dot_files/random/installed_explicitly.txt'
alias spr='pacman -Runs'

alias gc='git clone'
alias mci='make clean install'
alias mi='make install'

alias dfgc='bash /home/otis3761/code/git/dot_files/bin/dotfiles_git_commit.sh'
alias dfgp='bash /home/otis3761/code/git/dot_files/bin/dotfiles_git_push.sh'
alias dfga='bash /home/otis3761/code/git/dot_files/bin/dotfiles_git_add.sh'
alias dfgs='bash /home/otis3761/code/git/dot_files/bin/dotfiles_git_status.sh'
alias dotfiles='cd /home/otis3761/code/git/dot_files/'

alias subs='umount /mnt/baseStation'
alias v='nvim'
alias vim='nvim'
alias sc='sc-im'
alias br='$BROWSER & exit'
alias goo='google-chrome-stable & exit'
alias acd='source acd --BSD'


#Exports
export VISUAL="nvim"
export EDITOR="nvim"
export READER="zathura"
export TERMINAL="alacritty"
export TERM="alacritty"
export BROWSER="qutebrowser"
export PAGER="less"
export WM="dwm"
export HISTSIZE=10000
export HISTCONTROL=ignoreboth:erasedups
export HISTIGNORE="clear:exit"
export PATH=$PATH:/home/otis3761/code/git/dot_files/bin
export LESSHISTFILE=-
export LANG=C
export GPG_TTY

#Miscellaneous
shopt -s autocd 
complete -cf doas
GPG_TTY=$(tty)

PS1="\[\033[01;36m\]\D{[%H:%M} | \w ]\[\033[31m\]$\[\033[00m\] "
